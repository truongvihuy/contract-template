import { task } from 'hardhat/config';

task('balance', "Prints an account's balance")
  .addOptionalParam('address', "The account's address")
  .setAction(async (taskArgs, { ethers }) => {
    if (taskArgs.address) {
      const balance = await ethers.provider.getBalance(taskArgs.address);
      console.log(`${taskArgs.address}`, ethers.formatEther(balance), 'ETH');
    } else {
      const signers = await ethers.getSigners();
      await Promise.all(
        signers.map(async (sig) => {
          const balance = await ethers.provider.getBalance(sig.address);
          console.log(`${sig.address}`, ethers.formatEther(balance), 'ETH');
        }),
      );
    }
  });
