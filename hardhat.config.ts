import '@openzeppelin/hardhat-upgrades';
import dotenv from 'dotenv';
import { HardhatUserConfig } from 'hardhat/config';
import 'tsconfig-paths/register';

import '@nomicfoundation/hardhat-toolbox';
// npx hardhat node
// npx hardhat compile
// npx hardhat console
// npx hardhat coverage
// npx hardhat test
// npx hardhat ignition deploy <file-name> --network <network> --verify
// npx hardhat ignition verify <network>

import './toolbox';

dotenv.config();

const config: HardhatUserConfig = {
  solidity: {
    version: '0.8.28',
    settings: {
      optimizer: {
        enabled: true,
        runs: 1000,
      },
    },
  },
  gasReporter: {
    enabled: true,
  },
};

export default config;
